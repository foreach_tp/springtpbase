package com.example.paramtp;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Personne {

    private String nom, prenom;
    private int age;
}
