package com.example.paramtp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/base")
public class BaseRestController {

    @RequestMapping("/1-param")
    @ResponseBody
    public String param(){
        return "Attribut de test";
    }

    @RequestMapping("/params")
    @ResponseBody
    public List<String> collParam(){
        return  List.of("Att1","Att2","Att3","Att4","Att5","etc..");
    }
}
