package com.example.paramtp.controller;

import com.example.paramtp.Personne;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/base")
public class BaseController {

    @RequestMapping("/1-param")
    public String param(Model model){
        model.addAttribute("att", "Attribut de test");
        return "1-param";
    }

    @RequestMapping("/2-params")
    public String deuxParam(Model model){
        model.addAttribute("att", "Attribut de test");
        model.addAttribute("att2", "Attribut numéro 2");
        return "2-param";
    }

    @RequestMapping("/coll-params")
    public String collParam(Model model){
        List <String> listeAtt = List.of("Att1","Att2","Att3","Att4","Att5","etc..");
        model.addAttribute("liste", listeAtt);
        return "coll-params";
    }

    @RequestMapping("/personne")
    public String personne(Model model){
        Personne personne = new Personne("Dignoire","Thomas",23);
        model.addAttribute("p",personne);
        return "personne";
    }

}
