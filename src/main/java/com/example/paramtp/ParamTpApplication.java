package com.example.paramtp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParamTpApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParamTpApplication.class, args);
    }

}
